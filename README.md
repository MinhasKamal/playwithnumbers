# Play With Numbers
#### Making Fun With Numbers Through Programming

I love numbers a lot, and [these programs](http://minhaskamal.github.io/PlayWithNumbers) are examples of how much I really love them.

### Categorical List 

Numbers <br/>
&emsp;├─ factorial <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/factorial/BigFactorials.c">BigFactorials</a> <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/factorial/DigitsOfFactorial.c">DigitsOfFactorial</a> <br/>
&emsp;│&emsp;└─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/factorial/FactorsOfFactorial.c">FactorsOfFactorial</a> <br/>
&emsp;├─ fibonaciiNumber <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/fibonaciiNumber/FibonaciiNumber.c">FibonaciiNumber</a> <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/fibonaciiNumber/FibonaciiSerise.c">FibonaciiSerise</a> <br/>
&emsp;│&emsp;└─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/fibonaciiNumber/FibonaciiSum.c">FibonaciiSum</a> <br/>
&emsp;├─ otherNumbers <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/otherNumbers/FriendNumbers.c">FriendNumbers</a> <br/>
&emsp;│&emsp;└─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/otherNumbers/PerfectNumberHaunting.c">PerfectNumberHaunting</a> <br/>
&emsp;├─ pascalTriangle <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/pascalTriangle/BetterPascalTriangle.c">BetterPascalTriangle</a> <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/pascalTriangle/EasyPascalTriangle.c">EasyPascalTriangle</a> <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/pascalTriangle/PascalTriangle.c">PascalTriangle</a> <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/pascalTriangle/Piramid.c">Piramid</a> <br/>
&emsp;│&emsp;└─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/pascalTriangle/RealPascalTriangle.c">RealPascalTriangle</a> <br/>
&emsp;├─ primeNumber <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/primeNumber/AnotherWayOfPrimeNumberHaunting.c">AnotherWayOfPrimeNumberHaunting</a> <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/primeNumber/DefinitePrimeNumberHaunting.c">DefinitePrimeNumberHaunting</a> <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/primeNumber/Factors.c">Factors</a> <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/primeNumber/GreatPrimeNumberHaunting.c">GreatPrimeNumberHaunting</a> <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/primeNumber/GreatestPrimeNumberHaunting.c">GreatestPrimeNumberHaunting</a> <br/>
&emsp;│&emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/primeNumber/PrimeNumber.c">PrimeNumber</a> <br/>
&emsp;│&emsp;└─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/primeNumber/PrimeNumberHaunting.c">PrimeNumberHaunting</a> <br/>
&emsp;└─ time <br/>
&emsp; &emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/time/AgeCalculator.c">AgeCalculator</a> <br/>
&emsp; &emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/time/LeapYearCounter.c">LeapYearCounter</a> <br/>
&emsp; &emsp;├─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/time/Stopwatch.c">Stopwatch</a> <br/>
&emsp; &emsp;└─ <a href="https://github.com/MinhasKamal/PlayWithNumbers/blob/master/src/time/TimeAfter.c">TimeAfter</a> <br/>

### License
<a rel="license" href="http://www.gnu.org/licenses/gpl.html"><img alt="GNU General Public License" style="border-width:0" src="http://www.gnu.org/graphics/gplv3-88x31.png" /></a><br/>Play With Numbers is licensed under a <a rel="license" href="http://www.gnu.org/licenses/gpl.html">GNU General Public License version-3</a>.
